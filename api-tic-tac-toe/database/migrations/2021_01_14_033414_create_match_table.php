<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMatchTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('matches', function (Blueprint $table) {
            $table->id();
            $table->string('player_id');
            $table->string('player_name')->default('Player 1');
            $table->string('guest_id')->nullable();
            $table->string('guest_name')->default('Player 2');
            $table->boolean('finished')->default(false);
            $table->string('turn')->default('X');
            $table->string('start')->default('X');
            $table->string('winner')->nullable();
            $table->json('data')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('match');
    }
}
