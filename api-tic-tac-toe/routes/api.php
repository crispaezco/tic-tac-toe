<?php

use Illuminate\Support\Facades\Route;
use \App\Http\Controllers\MatchController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::apiResource('matches', MatchController::class)->only(['show', 'store', 'update']);
Route::put('matches/{match}/join', [MatchController::class, 'join']);
Route::put('matches/{match}/restart', [MatchController::class, 'restartMatch']);
Route::put('matches/{match}/change-player-name', [MatchController::class, 'changePlayerName']);
