<?php

namespace App\Http\Controllers;

use App\Events\TurnPlayed;
use App\Models\Match;
use Illuminate\Http\Request;

class MatchController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'player_id' => 'required',
        ]);

        $match = new Match();
        $match->player_id = $request->get('player_id');
        $match->save();
        return response($match);
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Models\Match $match
     * @return \Illuminate\Http\Response
     */
    public function show(Match $match)
    {
        return response($match);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Models\Match $match
     * @return \Illuminate\Http\Response
     */
    public function edit(Match $match)
    {
        //
    }

    /**
     * updates the game data with a player's move and sends a signal to be updated on the front.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Match $match
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Match $match)
    {
        $request->validate(['cell' => 'required', 'player_type' => 'required']);

        if ($match->turn !== $request->get('player_type')) {
            return response('Aun no es tu turno', '404');
        }
        if ($request->get('player_type') === 'X') {
            $match->turn = 'O';
        } else if ($request->get('player_type') === 'O') {
            $match->turn = 'X';
        }
        if (!isset($match->data[$request->get('cell')])) {
            $data = $match->data;
            $data[$request->get('cell')] = $request->get('player_type');
            $match->data = $data;
        }
        $match->checkHaveWinnerAndSave();
        TurnPlayed::dispatch($match);
        return response($match);
    }

    /**
     * Join to the match.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Match $match
     * @return \Illuminate\Http\Response
     */
    public function join(Request $request, Match $match)
    {
        $request->validate(['guest_id' => 'required']);
        $match->guest_id = $request->get('guest_id');
        $match->save();
        return response($match);
    }

    /**
     * Change player name.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Match $match
     * @return \Illuminate\Http\Response
     */
    public function changePlayerName(Request $request, Match $match)
    {
        $request->validate(['name' => 'required', 'player_type' => 'required']);
        if ($request->get('player_type') === 'X') {
            $match->player_name = $request->get('name');
        }
        if ($request->get('player_type') === 'O') {
            $match->guest_name = $request->get('name');
        }
        $match->save();
        TurnPlayed::dispatch($match);
        return response($match);
    }

    /**
     * Restart the match.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Match $match
     * @return \Illuminate\Http\Response
     */
    public function restartMatch(Request $request, Match $match)
    {
        if ($match->start === 'X') {
            $match->start = 'O';
        } else {
            $match->start = 'X';
        }
        $match->finished = false;
        $match->winner = '';
        $match->data = null;
        $match->save();
        TurnPlayed::dispatch($match);
        return response($match);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Models\Match $match
     * @return \Illuminate\Http\Response
     */
    public function destroy(Match $match)
    {
        //
    }
}
