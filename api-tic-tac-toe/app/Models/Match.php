<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Match extends Model
{
    use HasFactory;

    protected $casts = ['data' => 'array', 'finished' => 'boolean'];

    /**
     * the board positions that define a winner
     *
     * @var array
     */
    protected $positions_to_win = [
        //rows
        [1, 2, 3],
        [4, 5, 6],
        [7, 8, 9],

        //columns
        [1, 4, 7],
        [2, 5, 8],
        [3, 6, 9],

        //diagonals
        [1, 5, 9],
        [3, 5, 7],
    ];

    /**
     * check all the positions that the winner could define, finding a winning straight will set the game over and set the winner.
     *
     * @return void
     */
    public function checkHaveWinnerAndSave()
    {
        foreach ($this->positions_to_win as $lines) {
            if ($this->_checkCellMark($lines[0], 'X') && $this->_checkCellMark($lines[1], 'X') && $this->_checkCellMark($lines[2], 'X')) {
                $this->finished = true;
                $this->winner = 'Ganador ' . $this->player_name;
                break;
            }
            if ($this->_checkCellMark($lines[0], 'O') && $this->_checkCellMark($lines[1], 'O') && $this->_checkCellMark($lines[2], 'O')) {
                $this->finished = true;
                $this->winner = 'Ganador ' . $this->guest_name;
                break;
            }
        }
        if (!$this->finished) {
            if (count($this->data) === 9) {
                $this->finished = true;
                $this->winner = 'Empate';
            }
        }
        $this->save();
    }

    /**
     * check if the cell exists and if it was marked by a specific type of player.
     *
     * @param $cell string
     * @param $type string
     * @return boolean
     */
    private function _checkCellMark($cell, $type)
    {
        return isset($this->data[$cell]) && $this->data[$cell] === $type;
    }
}
