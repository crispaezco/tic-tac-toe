# Montechelo
> Prueba técnica - Desarrollador Full Stack

## Dependencias 
* MySQL: ^5.7
* PHP: ^7.3|^8.0
* Composer: ^1.10.0
* Node.js: ^12.19.0

## Instalación de API (Back en Laravel)
```sh
$ cd api-tic-tac-toe
$ composer install
```
#### Variables de entorno 
Se compartira un archivo .env con las variables necesarias para funcionar, igualmente podra habilitar una copia de .env.example que contara con la misma version de desarrollo. Por favor tener en cuenta las siguientes variables para conectar a su base de datos.
```
...
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE=database_name
DB_USERNAME=root
DB_PASSWORD=
...
```

#### Migraciones de base de datos
El comando `migrate` se debe ejecutar únicamente después de configurar las variables de entorno correspondientes a su base de datos.
* ##### Migración
```sh
$ php artisan migrate
```

### Iniciar Servidor
Por defecto se iniciará en la ruta [localhost:8000](http://127.0.0.1:8000)
```sh
$ php artisan serve
```


## Instalación de Cliente
```sh
$ cd front-tic-tac-toe
$ npm i -g @angular/cli
$ npm i
```

### Iniciar Cliente
Por defecto se iniciará en la ruta [localhost:4200](http://localhost:4200)
```sh
$ ng serve --o
```
## Uso de Aplicación
Para una correcta experiencia debe abrir la url en dos diferentes navegadores, en uno de ellos dara en la opción de nueva partida y en la otra debera dar click en la opción de unirme, alli colocara el id de la partida que podra ver en la url de la partida del navegador 1, o en su defecto copiar y pegar la misma url en el nuevo navegador. los movimientos realizados en un navegador se veran automaticamente reflejados en el otro navegador. Para esto ultimo se implemento broadcasting con pusher.
