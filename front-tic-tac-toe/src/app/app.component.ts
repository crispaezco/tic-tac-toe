import {Component} from '@angular/core';
import * as uuid from 'uuid';
import {LocalStorageService} from 'ngx-localstorage';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.sass']
})
export class AppComponent {
  constructor(private storageService: LocalStorageService) {
    const myId = uuid.v4();
    this.storageService.asPromisable().get('player_id').then(data => {
      if (data === null) {
        this.storageService.set('player_id', myId);
      }
    });
  }
}
