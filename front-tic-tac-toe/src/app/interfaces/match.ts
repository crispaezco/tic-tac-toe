export interface Match {
  id: number;
  player_id: string;
  player_name: string;
  guest_id: string;
  guest_name: string;
  finished: boolean;
  board_size: number;
  turn: string;
  winner: string;
  data: any;
}
