import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Match} from '../interfaces/match';
import {environment} from '../../environments/environment';
import Pusher from 'pusher-js';

@Injectable({
  providedIn: 'root'
})
export class MatchesService {
  pusher: any;
  channel: any;

  constructor(private http: HttpClient) {
  }

  /**
   * subscribe to the match channel.
   *
   */
  channelSubscribe(matchId: number): void {
    this.pusher = new Pusher(environment.pusher.key, {
      cluster: environment.pusher.cluster
    });
    this.channel = this.pusher.subscribe(`matches-channel.${matchId}`);
  }

  /**
   * create a new match.
   *
   */
  store(playerId: string): Observable<Match> {
    return this.http.post<Match>(`${environment.endpoint}api/matches`, {player_id: playerId});
  }

  /**
   * get the information of a match by the id.
   *
   */
  getById(id: number): Observable<Match> {
    return this.http.get<Match>(`${environment.endpoint}api/matches/${id}`);
  }

  /**
   * send the information of the turn played.
   *
   */
  playTurn(id: number, playerOrGuestId: string, playerType: string, cell: string): Observable<Match> {
    return this.http.put<Match>(`${environment.endpoint}api/matches/${id}`, {
      cell,
      player_or_guest_id: playerOrGuestId,
      player_type: playerType
    });
  }

  /**
   * allows a guest to join the match.
   *
   */
  joinToMatch(id: number, guestId: string): Observable<Match> {
    return this.http.put<Match>(`${environment.endpoint}api/matches/${id}/join`, {
      guest_id: guestId,
    });
  }

  /**
   * allows a player to change their name.
   *
   */
  setPlayerName(id: number, name: string, type: string): Observable<Match> {
    return this.http.put<Match>(`${environment.endpoint}api/matches/${id}/change-player-name`, {
      name,
      player_type: type
    });
  }

  /**
   * allows you to restart the match.
   *
   */
  restartMatch(id: number): Observable<Match> {
    return this.http.put<Match>(`${environment.endpoint}api/matches/${id}/restart`, {});
  }
}
