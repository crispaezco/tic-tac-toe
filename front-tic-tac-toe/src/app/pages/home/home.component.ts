import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {FormControl} from '@angular/forms';
import {MatchesService} from '../../services/matches.service';
import {LocalStorageService} from 'ngx-localstorage';
import {environment} from '../../../environments/environment';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.sass']
})
export class HomeComponent implements OnInit {

  gameId = new FormControl('');

  constructor(private router: Router, private modalService: NgbModal, private matchService: MatchesService, private storageService: LocalStorageService) {
  }

  ngOnInit(): void {
  }

  /**
   * calls the service to create a new match for the current user.
   *
   */
  newGame(): void {
    this.storageService.asPromisable().get('player_id').then(playerId => {
      this.matchService.store(playerId)
        .subscribe(match => {
          this.router.navigate([`game/${match.id}`]);
        });
    });
  }

  /**
   * lift the modal to join the match.
   *
   */
  openJoinModal(content) {
    this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title'}).result.then((result) => {
      console.log(result);
    }, (reason) => {
      console.log(reason);
    });
  }

  /**
   * allows the user to join a match through the service.
   *
   */
  joinTheGame(): void {
    this.modalService.dismissAll();
    this.storageService.asPromisable().get('player_id').then(playerId => {
      this.matchService.joinToMatch(this.gameId.value, playerId)
        .subscribe(match => this.router.navigate([`game/${match.id}`]));
    });

  }

}
