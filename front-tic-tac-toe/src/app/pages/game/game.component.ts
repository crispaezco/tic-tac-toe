import {Component, OnInit} from '@angular/core';
import {MatchesService} from '../../services/matches.service';
import {Match} from '../../interfaces/match';
import {ActivatedRoute, Router} from '@angular/router';
import {Observable} from 'rxjs';
import {LocalStorageService} from 'ngx-localstorage';
import {FormControl} from '@angular/forms';

@Component({
  selector: 'app-game',
  templateUrl: './game.component.html',
  styleUrls: ['./game.component.sass']
})
export class GameComponent implements OnInit {

  playerName = new FormControl('');
  match: Match;
  boardSize = 3;
  board = [];
  playerId: string;
  turnControl = true;

  constructor(private matchService: MatchesService, private route: ActivatedRoute, private storageService: LocalStorageService) {
    this.board = Array(this.boardSize * this.boardSize).fill(this.boardSize * this.boardSize).map((x, i) => i + 1);
  }

  /**
   * get the id of the current player, load the information of the match, subscribe and listen to events of the match.
   *
   */
  ngOnInit(): void {
    this.playerId = this.storageService.get('player_id');
    const matchId = this.route.snapshot.paramMap.get('id');
    this.loadMatchData(+matchId);
    this.matchService.channelSubscribe(+matchId);
    this.listenEvent();
  }

  /**
   * listen to the event of the turn played that transmits the channel of the match.
   *
   */
  listenEvent(): void {
    this.matchService.channel.bind('App\\Events\\TurnPlayed', (data: any) => {
      this.match = data.match;
      this.match.data = (this.match.data !== null) ? this.match.data : {};
    });
  }

  /**
   * get the match information.
   *
   */
  loadMatchData(matchId: number): void {
    this.matchService.getById(matchId)
      .subscribe(match => {
        this.match = match;
        this.match.data = (match.data !== null) ? match.data : {};
        if (this.amiGuest()) {
          this.playerName.setValue(this.match.guest_name);
        } else {
          this.playerName.setValue(this.match.player_name);
        }
      });
  }

  /**
   * play the user's turn.
   *
   */
  myTurn(cell): void {
    if (this.canPlay() && !this.match.data[cell]) {
      this.turnControl = false;
      this.match.data[cell] = this.amiGuest() ? 'O' : 'X';
      this.matchService.playTurn(this.match.id, this.playerId, this.getMyPlayerType(), cell)
        .subscribe((match: Match) => {
          this.turnControl = true;
          console.log(match);
        });

    } else {
      alert('No puedes marcar esta casilla');
    }
  }

  /**
   * through the service changes the player's name.
   *
   */
  changePlayerName(): void {
    this.matchService.setPlayerName(this.match.id, this.playerName.value, this.getMyPlayerType())
      .subscribe((match: Match) => {
        //
      });
  }

  /**
   * validate if you can play the turn.
   *
   */
  canPlay(): boolean {
    if (this.amiGuest()) {
      return this.match.turn === 'O' && this.turnControl && !this.match.finished;
    } else {
      return this.match.turn === 'X' && this.turnControl && !this.match.finished;
    }
  }

  /**
   * valid if you are a guest user in the current match.
   *
   */
  amiGuest(): boolean {
    return this.getMyPlayerType() !== 'X';
  }

  /**
   * restart the match.
   *
   */
  restart(): void {
    this.matchService.restartMatch(this.match.id)
      .subscribe((match: Match) => {
        location.href = `/game/${match.id}`;
      });
  }

  /**
   * get the symbol of my player type in the current match.
   *
   */
  getMyPlayerType(): string {
    if (this.match.player_id === this.playerId) {
      return 'X';
    } else {
      return 'O';
    }

  }

}
